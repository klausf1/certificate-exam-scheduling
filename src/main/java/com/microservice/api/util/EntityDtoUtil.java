package com.microservice.api.util;

import com.microservice.api.domain.*;
import com.microservice.api.dto.availability.AvailabilityResponseDto;
import com.microservice.api.dto.candidate.CandidateResponseDto;
import com.microservice.api.dto.exam.ExamResponseDto;
import com.microservice.api.dto.room.RoomResponseDto;
import com.microservice.api.dto.scheduling.SchedulingResponseDto;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.nonNull;

public class EntityDtoUtil {

    public static AvailabilityResponseDto getAvailabilityResponseDto(Availability availability) {
        var dto = AvailabilityResponseDto.builder().build();

        if (nonNull(availability.getRoom())) {
            var room = availability.getRoom();
            var roomDto = RoomResponseDto.builder()
                    .id(room.getId())
                    .name(room.getName())
                    .location(room.getLocation())
                    .build();
            dto.setRoom(roomDto);
        }

        BeanUtils.copyProperties(availability, dto);
        dto.setId(availability.getId());

        return dto;
    }

    public static CandidateResponseDto getCandidateResponseDto(Candidate candidate) {
        var dto = CandidateResponseDto.builder().build();
        BeanUtils.copyProperties(candidate, dto);
        dto.setId(candidate.getId());
        return dto;
    }

    public static ExamResponseDto getExamResponseDto(Exam exam) {
        var dto = ExamResponseDto.builder().build();

        if (nonNull(exam.getExamAvaliabilities())) {
            var availabilities = exam.getExamAvaliabilities().stream().map(examAvaliability -> examAvaliability.getExamAvailabilityId().getAvailability())
                    .map(EntityDtoUtil::getAvailabilityResponseDto)
                    .collect(Collectors.toList());
            dto.setAvailabilities(availabilities);
        }

        BeanUtils.copyProperties(exam, dto);
        dto.setId(exam.getId());
        return dto;
    }

    public static RoomResponseDto getRoomResponseDto(Room room) {
        return RoomResponseDto.builder()
                .id(room.getId())
                .name(room.getName())
                .number(room.getNumber())
                .capacity(room.getCapacity())
                .location(room.getLocation())
                .availabilities(convertList(room.getAvailabilities()))
                .build();
    }

    private static List<AvailabilityResponseDto> convertList(List<Availability> avaliabilities) {
        if (nonNull(avaliabilities)) {
            return avaliabilities.stream().map(EntityDtoUtil::convert)
                    .collect(Collectors.toList());
        }
        return null;
    }

    private static AvailabilityResponseDto convert(Availability availability) {
        var dto = AvailabilityResponseDto.builder().build();
        BeanUtils.copyProperties(availability, dto);
        dto.setId(availability.getId());
        return dto;
    }

    public static SchedulingResponseDto getSchedulingResponseDto(SchedulingExam schedulingExam) {
        var dto = SchedulingResponseDto.builder().build();
        BeanUtils.copyProperties(schedulingExam, dto);
        dto.setId(schedulingExam.getId());
        dto.setCandidate(CandidateResponseDto.builder()
                .id(schedulingExam.getCandidate().getId())
                .documentNumber(schedulingExam.getCandidate().getDocumentNumber())
                .fullName(schedulingExam.getCandidate().getFullName())
                .build());
        dto.setExam(schedulingExam.getExam().getName());

        if (nonNull(schedulingExam.getRoom())) {
            dto.setRoom(RoomResponseDto.builder()
                    .name(schedulingExam.getRoom().getName())
                    .location(schedulingExam.getRoom().getLocation())
                    .build());
        }

        return dto;
    }
}
