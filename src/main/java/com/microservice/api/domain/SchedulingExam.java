package com.microservice.api.domain;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Audited
@Data
@NoArgsConstructor
@AllArgsConstructor(staticName = "create")
@Entity
@Table
public class SchedulingExam {

    @Id
    @Column(name = "ID", nullable = false)
    private String id;

    @Column(name = "CREATED_DATE", nullable = false)
    private LocalDateTime createdDate;

    @Column(name = "EXAM_DATE", nullable = false)
    private LocalDate examDate;

    @Column(name = "PERIOD", nullable = false)
    private String period;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CANDIDATE_ID", referencedColumnName = "ID")
    private Candidate candidate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "EXAME_ID", referencedColumnName = "ID")
    private Exam exam;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ROOM_ID", referencedColumnName = "ID")
    private Room room;
}
