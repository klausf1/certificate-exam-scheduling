package com.microservice.api.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.util.List;

@Audited
@Data
@NoArgsConstructor
@AllArgsConstructor(staticName = "create")
@Entity
@Table
public class Room {

    @Id
    @Column(name = "ID", nullable = false)
    private String id;

    @Column(name = "NAME", nullable = false, unique = true)
    private String name;

    @Column(name = "NUMBER", nullable = false)
    private Integer number;

    @Column(name = "CAPACITY", nullable = false)
    private Integer capacity;

    @Column(name = "LOCATION", nullable = false)
    private String location;

    @OneToMany(mappedBy = "room", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Availability> availabilities;
}
