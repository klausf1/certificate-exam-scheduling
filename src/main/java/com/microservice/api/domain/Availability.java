package com.microservice.api.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.time.LocalDate;

@Audited
@Data
@NoArgsConstructor
@AllArgsConstructor(staticName = "create")
@Entity
@Table
public class Availability {

    @Id
    private String id;

    @Column(name = "AVAILABLE_DATE", nullable = false)
    private LocalDate availableDate;

    @Column(name = "PERIOD", nullable = false)
    private String period;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "EXAM_ID", referencedColumnName = "ID")
    private Exam exam;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ROOM_ID", referencedColumnName = "ID")
    private Room room;
}
