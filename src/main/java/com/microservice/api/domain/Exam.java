package com.microservice.api.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.util.List;

@Audited
@Data
@NoArgsConstructor
@AllArgsConstructor(staticName = "create")
@Entity
@Table
public class Exam {

    @Id
    @Column(name = "ID", nullable = false)
    private String id;

    @Column(name = "NAME", nullable = false, unique = true)
    private String name;

    @Column(name = "NUMBER_QUESTIONS", nullable = false)
    private Integer numberOfQuestions;

    @Column(name = "PASSING_GRADE", nullable = false)
    private Integer passingGrade;

    @OneToMany(mappedBy = "examAvailabilityId.exam", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<ExamAvaliability> examAvaliabilities;
}
