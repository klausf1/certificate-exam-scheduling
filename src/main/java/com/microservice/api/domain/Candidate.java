package com.microservice.api.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.envers.Audited;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Audited
@Data
@NoArgsConstructor
@AllArgsConstructor(staticName = "create")
@Entity
@Table
public class Candidate {

    @Id
    @Column(name = "ID", nullable = false)
    private String id;

    @Column(name = "DOCUMENT_NUMBER", nullable = false, unique = true)
    private String documentNumber;

    @Column(name = "FULL_NAME", nullable = false)
    private String fullName;

    @Column(name = "USERNAME", nullable = false, unique = true)
    private String username;

    @Column(name = "BIRTH_DATE", nullable = false)
    private LocalDate birthDate;

    @Column(name = "REGISTRATION_DATE", nullable = false)
    private LocalDateTime registrationDate;
}
