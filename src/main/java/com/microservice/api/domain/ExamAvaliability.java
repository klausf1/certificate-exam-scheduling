package com.microservice.api.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.envers.Audited;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Audited
@Data
@NoArgsConstructor
@AllArgsConstructor(staticName = "create")
@Entity
@Table
public class ExamAvaliability {

    @EmbeddedId
    private ExamAvailabilityId examAvailabilityId;

    @Column(name = "CREATE_DATE")
    private LocalDateTime createdDate;
}
