package com.microservice.api.app.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@AllArgsConstructor
public class CertificateExamException extends RuntimeException {

    private static final long serialVersionUID = -4293569759069796130L;

    private final BusinessErrorDTO businessErrorDTO;
}
