package com.microservice.api.app.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.support.WebExchangeBindException;
import reactor.core.publisher.Mono;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Slf4j
@ControllerAdvice
public class ExceptionControl {

    public static final String RESPONSE_DEFAULT_MESSAGE = "Response: {}";

    public static final String GENERIC_VALIDATION_ERROR_CODE = "ERR-00001";

    public static final String GENERIC_VALIDATION_ERROR_MESSAGE = "An error occurred while validating the request data";

    private final MessageSource messageSource;

    public ExceptionControl(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @ExceptionHandler(BusinessException.class)
    public Mono<ResponseEntity<BusinessErrorDTO>> handle(BusinessException businessException) {
        return Mono.just(ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(BusinessErrorDTO.create(businessException.getMessage())))
                .doOnSuccess(errorResponseEntity -> log.error(RESPONSE_DEFAULT_MESSAGE, errorResponseEntity));
    }

    @ExceptionHandler(Throwable.class)
    public Mono<ResponseEntity<BusinessErrorDTO>> handle(Throwable throwable) {
        log.error("Error: {}", throwable.getMessage(), throwable);

        var businessErrorDTO = BusinessErrorDTO.create(messageSource.getMessage("unexpected-error", null,
                LocaleContextHolder.getLocale()));

        return Mono.just(ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(businessErrorDTO))
                .doOnSuccess(errorResponseEntity -> log.error(RESPONSE_DEFAULT_MESSAGE, errorResponseEntity));
    }

    @ExceptionHandler(WebExchangeBindException.class)
    public ResponseEntity<ErrorDTO> webExchangeBindExceptionHandler(WebExchangeBindException exception,
                                                                    ServerHttpRequest request) {
        return ResponseEntity
                .badRequest()
                .body(convert(request, GENERIC_VALIDATION_ERROR_MESSAGE, extractErrorDetails(exception)));
    }

    private ErrorDTO convert(ServerHttpRequest request, String message, List<ErrorDetailDTO> details) {
        return ErrorDTO.builder()
                .code(GENERIC_VALIDATION_ERROR_CODE)
                .error(HttpStatus.BAD_REQUEST.getReasonPhrase())
                .status(HttpStatus.BAD_REQUEST.value())
                .message(message)
                .timestamp(ZonedDateTime.now())
                .path(request.getURI().getRawPath())
                .details(details)
                .build();
    }

    private List<ErrorDetailDTO> extractErrorDetails(WebExchangeBindException exception) {
        return exception.getAllErrors()
                .stream()
                .filter(Objects::nonNull)
                .map(this::convert)
                .collect(Collectors.toList());

    }

    private ErrorDetailDTO convert(ObjectError from) {
        return ErrorDetailDTO.builder()
                .field(from instanceof FieldError ? ((FieldError) from).getField() : null)
                .key(from.getDefaultMessage())
                .message(getFieldErrorMessage(from))
                .build();
    }

    private String getFieldErrorMessage(ObjectError fieldError) {
        return this.messageSource.getMessage(
                Objects.requireNonNull(fieldError.getDefaultMessage()),
                fieldError.getArguments(),
                LocaleContextHolder.getLocale());
    }
}