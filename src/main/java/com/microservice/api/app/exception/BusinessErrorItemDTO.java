package com.microservice.api.app.exception;

import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class BusinessErrorItemDTO {

    private String id;

    private String message;
}
