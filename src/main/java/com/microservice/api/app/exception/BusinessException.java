package com.microservice.api.app.exception;

public class BusinessException extends RuntimeException {

    private static final long serialVersionUID = 1L;
    private final String code;

    public BusinessException(String message, Throwable cause) {
        this(null, message, cause);
    }

    public BusinessException(String code, String message, Throwable cause) {
        super(message, cause);
        this.code = code;
    }

    public BusinessException(String message) {
        this(null, message);
    }

    public BusinessException(String code, String message) {
        super(message);
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
