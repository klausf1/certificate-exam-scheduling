package com.microservice.api.controller;

import com.microservice.api.app.exception.BusinessErrorDTO;
import com.microservice.api.app.exception.ErrorDTO;
import com.microservice.api.dto.exam.ExamRequestDto;
import com.microservice.api.dto.exam.ExamResponseDto;
import com.microservice.api.service.ExamService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("v1/exam")
@RequiredArgsConstructor
public class ExamController {

    private final ExamService service;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Enpoint listing exams")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Request processed successfully", response = ExamResponseDto.class),
            @ApiResponse(code = 400, message = "Invalid requisition", response = ErrorDTO.class),
            @ApiResponse(code = 500, message = "Error processing request", response = BusinessErrorDTO.class)
    })
    public Mono<ResponseEntity<List<ExamResponseDto>>> listAll() {
        return service.findAll()
                .map(ResponseEntity::ok);
    }

    @GetMapping(path = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Endpoint that lists an exam by an id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Request processed successfully", response = ExamResponseDto.class),
            @ApiResponse(code = 400, message = "Invalid requisition", response = ErrorDTO.class),
            @ApiResponse(code = 500, message = "Error processing request", response = BusinessErrorDTO.class)
    })
    public Mono<ResponseEntity<ExamResponseDto>> findById(@PathVariable String id) {
        return service.findById(id)
                .map(ResponseEntity::ok)
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Endpoint that saves an exam")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Request processed successfully", response = ExamResponseDto.class),
            @ApiResponse(code = 400, message = "Invalid requisition", response = ErrorDTO.class),
            @ApiResponse(code = 500, message = "Error processing request", response = BusinessErrorDTO.class)
    })
    public Mono<ResponseEntity<ExamResponseDto>> save(@Valid @RequestBody ExamRequestDto request) {
        return service.save(request)
                .map(examResponseDto -> ResponseEntity.status(HttpStatus.CREATED).body(examResponseDto));
    }

    @PutMapping(path = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @ApiOperation(value = "Endpoint that updates an exam")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Request processed successfully", response = ExamResponseDto.class),
            @ApiResponse(code = 400, message = "Invalid requisition", response = ErrorDTO.class),
            @ApiResponse(code = 500, message = "Error processing request", response = BusinessErrorDTO.class)
    })
    public Mono<ResponseEntity<ExamResponseDto>> update(@PathVariable String id,
                                                        @Valid @RequestBody ExamRequestDto request) {
        return service.update(id, request)
                .map(candidateResponseDto -> ResponseEntity.status(HttpStatus.NO_CONTENT).body(candidateResponseDto))
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @DeleteMapping(path = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Endpoint that removes an exam")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Request processed successfully"),
            @ApiResponse(code = 400, message = "Invalid requisition", response = ErrorDTO.class),
            @ApiResponse(code = 500, message = "Error processing request", response = BusinessErrorDTO.class)
    })
    public Mono<Void> delete(@PathVariable String id) {
        return service.delete(id);
    }
}
