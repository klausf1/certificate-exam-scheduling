package com.microservice.api.controller;

import com.microservice.api.app.exception.BusinessErrorDTO;
import com.microservice.api.app.exception.ErrorDTO;
import com.microservice.api.dto.scheduling.SchedulingRequestDto;
import com.microservice.api.dto.scheduling.SchedulingResponseDto;
import com.microservice.api.service.SchedulingService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("v1/scheduling")
@RequiredArgsConstructor
public class SchedulingController {

    private final SchedulingService service;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Enpoint listing exam certificate schedules")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Request processed successfully", response = SchedulingResponseDto.class),
            @ApiResponse(code = 400, message = "Invalid requisition", response = ErrorDTO.class),
            @ApiResponse(code = 500, message = "Error processing request", response = BusinessErrorDTO.class)
    })
    public Mono<ResponseEntity<List<SchedulingResponseDto>>> listAll() {
        return service.findAll()
                .map(ResponseEntity::ok);
    }

    @GetMapping(path = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Enpoint that lists an exam certificate schedule by id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Request processed successfully", response = SchedulingResponseDto.class),
            @ApiResponse(code = 400, message = "Invalid requisition", response = ErrorDTO.class),
            @ApiResponse(code = 500, message = "Error processing request", response = BusinessErrorDTO.class)
    })
    public Mono<ResponseEntity<SchedulingResponseDto>> findById(@PathVariable String id) {
        return service.findById(id)
                .map(ResponseEntity::ok)
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Enpoint that saves an exam certificate schedule")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Request processed successfully", response = SchedulingResponseDto.class),
            @ApiResponse(code = 400, message = "Invalid requisition", response = ErrorDTO.class),
            @ApiResponse(code = 500, message = "Error processing request", response = BusinessErrorDTO.class)
    })
    public Mono<ResponseEntity<String>> save(@Valid @RequestBody SchedulingRequestDto request) {
        return service.save(request)
                .map(examResponseDto -> ResponseEntity.status(HttpStatus.CREATED).body(examResponseDto));
    }

    @DeleteMapping(path = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Enpoint that removes an exam certificate schedule")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Request processed successfully", response = SchedulingResponseDto.class),
            @ApiResponse(code = 400, message = "Invalid requisition", response = ErrorDTO.class),
            @ApiResponse(code = 500, message = "Error processing request", response = BusinessErrorDTO.class)
    })
    public Mono<Void> delete(@PathVariable String id) {
        return service.delete(id);
    }
}
