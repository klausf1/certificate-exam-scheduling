package com.microservice.api.controller;

import com.microservice.api.app.exception.BusinessErrorDTO;
import com.microservice.api.app.exception.ErrorDTO;
import com.microservice.api.dto.room.RoomRequestDto;
import com.microservice.api.dto.room.RoomResponseDto;
import com.microservice.api.service.RoomService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("v1/room")
@RequiredArgsConstructor
public class RoomController {

    private final RoomService service;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Enpoint that lists the rooms")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Request processed successfully", response = RoomResponseDto.class),
            @ApiResponse(code = 400, message = "Invalid requisition", response = ErrorDTO.class),
            @ApiResponse(code = 500, message = "Error processing request", response = BusinessErrorDTO.class)
    })
    public Mono<ResponseEntity<List<RoomResponseDto>>> listAll() {
        return service.findAll()
                .map(ResponseEntity::ok);
    }

    @GetMapping(path = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Endpoint that lists a room by an id")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Request processed successfully", response = RoomResponseDto.class),
            @ApiResponse(code = 400, message = "Invalid requisition", response = ErrorDTO.class),
            @ApiResponse(code = 500, message = "Error processing request", response = BusinessErrorDTO.class)
    })
    public Mono<ResponseEntity<RoomResponseDto>> findById(@PathVariable String id) {
        return service.findById(id)
                .map(ResponseEntity::ok)
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Endpoint that saves a room")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Request processed successfully", response = RoomResponseDto.class),
            @ApiResponse(code = 400, message = "Invalid requisition", response = ErrorDTO.class),
            @ApiResponse(code = 500, message = "Error processing request", response = BusinessErrorDTO.class)
    })
    public Mono<ResponseEntity<RoomResponseDto>> save(@Valid @RequestBody RoomRequestDto request) {
        return service.save(request)
                .map(examResponseDto -> ResponseEntity.status(HttpStatus.CREATED).body(examResponseDto));
    }

    @PutMapping(path = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @ApiOperation(value = "Endpoint that updates a room")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Request processed successfully", response = RoomResponseDto.class),
            @ApiResponse(code = 400, message = "Invalid requisition", response = ErrorDTO.class),
            @ApiResponse(code = 500, message = "Error processing request", response = BusinessErrorDTO.class)
    })
    public Mono<ResponseEntity<RoomResponseDto>> update(@PathVariable String id,
                                                        @Valid @RequestBody RoomRequestDto request) {
        return service.update(id, request)
                .map(candidateResponseDto -> ResponseEntity.status(HttpStatus.NO_CONTENT).body(candidateResponseDto))
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @DeleteMapping(path = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Endpoint that removes a room")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Request processed successfully"),
            @ApiResponse(code = 400, message = "Invalid requisition", response = ErrorDTO.class),
            @ApiResponse(code = 500, message = "Error processing request", response = BusinessErrorDTO.class)
    })
    public Mono<Void> delete(@PathVariable String id) {
        return service.delete(id);
    }
}
