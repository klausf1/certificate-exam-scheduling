package com.microservice.api.controller;

import com.microservice.api.app.exception.BusinessErrorDTO;
import com.microservice.api.app.exception.ErrorDTO;
import com.microservice.api.dto.candidate.CandidateRequestDto;
import com.microservice.api.dto.candidate.CandidateResponseDto;
import com.microservice.api.service.CandidateService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("v1/candidate")
@RequiredArgsConstructor
public class CandidateController {

    private final CandidateService service;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Endpoint that lists candidates")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Request processed successfully", response = CandidateResponseDto.class),
            @ApiResponse(code = 400, message = "Invalid requisition", response = ErrorDTO.class),
            @ApiResponse(code = 500, message = "Error processing request", response = BusinessErrorDTO.class)
    })
    public Mono<ResponseEntity<List<CandidateResponseDto>>> listAll() {
        return service.findAll()
                .map(ResponseEntity::ok);
    }

    @GetMapping(path = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Endpoint that lists candidates")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Request processed successfully", response = CandidateResponseDto.class),
            @ApiResponse(code = 400, message = "Invalid requisition", response = ErrorDTO.class),
            @ApiResponse(code = 500, message = "Error processing request", response = BusinessErrorDTO.class)
    })
    public Mono<ResponseEntity<CandidateResponseDto>> findById(@PathVariable String id) {
        return service.findById(id)
                .map(ResponseEntity::ok)
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Endpoint that saves a candidate")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Request processed successfully", response = CandidateResponseDto.class),
            @ApiResponse(code = 400, message = "Invalid requisition", response = ErrorDTO.class),
            @ApiResponse(code = 500, message = "Error processing request", response = BusinessErrorDTO.class)
    })
    public Mono<ResponseEntity<CandidateResponseDto>> save(@Valid @RequestBody CandidateRequestDto request) {
        return service.save(request)
                .map(candidateResponseDto -> ResponseEntity.status(HttpStatus.CREATED).body(candidateResponseDto));
    }

    @PutMapping(path = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Endpoint that updates a candidate")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Request processed successfully", response = CandidateResponseDto.class),
            @ApiResponse(code = 400, message = "Invalid requisition", response = ErrorDTO.class),
            @ApiResponse(code = 500, message = "Error processing request", response = BusinessErrorDTO.class)
    })
    public Mono<ResponseEntity<CandidateResponseDto>> update(@PathVariable String id,
                                                             @Valid @RequestBody CandidateRequestDto request) {
        return service.update(id, request)
                .map(candidateResponseDto -> ResponseEntity.status(HttpStatus.NO_CONTENT).body(candidateResponseDto))
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @DeleteMapping(path = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Endpoint that removes a candidate")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Request processed successfully"),
            @ApiResponse(code = 400, message = "Invalid requisition", response = ErrorDTO.class),
            @ApiResponse(code = 500, message = "Error processing request", response = BusinessErrorDTO.class)
    })
    public Mono<Void> delete(@PathVariable String id) {
        return service.delete(id);
    }
}
