package com.microservice.api.repository;

import com.microservice.api.domain.Availability;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.Optional;

public interface AvailabilityRepository extends JpaRepository<Availability, String> {

    Optional<Availability> findByAvailableDateIsAndPeriodIs(LocalDate availableDate, String periodo);
}
