package com.microservice.api.repository;

import com.microservice.api.domain.Exam;
import com.microservice.api.domain.SchedulingExam;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.Optional;

public interface SchedulingExamRepository extends JpaRepository<SchedulingExam, String> {

    Optional<SchedulingExam> findByExamDateIsAndPeriodIsAndExamIs(LocalDate examDate,
                                                                  String period,
                                                                  Exam exam);
}
