package com.microservice.api.service;

import com.microservice.api.domain.Candidate;
import com.microservice.api.dto.candidate.CandidateRequestDto;
import com.microservice.api.dto.candidate.CandidateResponseDto;
import com.microservice.api.repository.CandidateRepository;
import com.microservice.api.util.EntityDtoUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
@Transactional
public class CandidateService {

    private final CandidateRepository candidateRepository;

    public Mono<List<CandidateResponseDto>> findAll() {
        return Flux.fromIterable(candidateRepository.findAll())
                .map(EntityDtoUtil::getCandidateResponseDto)
                .collectList()
                .subscribeOn(Schedulers.boundedElastic());
    }

    public Mono<CandidateResponseDto> findById(String id) {
        return Mono.fromCallable(() -> candidateRepository.findById(id).orElse(null))
                .map(EntityDtoUtil::getCandidateResponseDto)
                .subscribeOn(Schedulers.boundedElastic());
    }

    public Mono<CandidateResponseDto> save(CandidateRequestDto request) {
        return Mono.fromCallable(() -> candidateRepository.save(convert(request)))
                .map(EntityDtoUtil::getCandidateResponseDto)
                .subscribeOn(Schedulers.boundedElastic());
    }

    public Mono<CandidateResponseDto> update(String id, CandidateRequestDto request) {
        return Mono.fromCallable(() -> call(id, request))
                .map(EntityDtoUtil::getCandidateResponseDto)
                .subscribeOn(Schedulers.boundedElastic());
    }

    public Mono<Void> delete(String id) {
        return Mono.fromRunnable(() -> callExclusion(id))
                .then();
    }

    private void callExclusion(String id) {
        var candidateConsult = candidateRepository.findById(id);
        candidateConsult.ifPresent(this.candidateRepository::delete);
    }

    private Candidate convert(CandidateRequestDto request) {
        return Candidate.create(
                UUID.randomUUID().toString(),
                request.getDocumentNumber(),
                request.getFullName(),
                request.getUsername(),
                request.getBirthDate(),
                LocalDateTime.now());
    }

    private Candidate call(String id, CandidateRequestDto request) {
        return candidateRepository.findById(id)
                .map(candidate -> Candidate.create(
                        candidate.getId(),
                        request.getDocumentNumber(),
                        request.getFullName(),
                        request.getUsername(),
                        request.getBirthDate(),
                        candidate.getRegistrationDate()))
                .map(this.candidateRepository::save)
                .orElse(null);
    }
}
