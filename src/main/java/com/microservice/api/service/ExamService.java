package com.microservice.api.service;

import com.microservice.api.domain.Exam;
import com.microservice.api.domain.ExamAvailabilityId;
import com.microservice.api.domain.ExamAvaliability;
import com.microservice.api.dto.exam.AvailabilityRequest;
import com.microservice.api.dto.exam.ExamRequestDto;
import com.microservice.api.dto.exam.ExamResponseDto;
import com.microservice.api.repository.AvailabilityRepository;
import com.microservice.api.repository.ExamRepository;
import com.microservice.api.util.EntityDtoUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static java.util.Objects.nonNull;

@Service
@RequiredArgsConstructor
@Transactional
public class ExamService {

    private final ExamRepository examRepository;
    private final AvailabilityRepository availabilityRepository;

    public Mono<List<ExamResponseDto>> findAll() {
        return Flux.fromIterable(examRepository.findAll())
                .map(EntityDtoUtil::getExamResponseDto)
                .collectList()
                .subscribeOn(Schedulers.boundedElastic());
    }

    public Mono<ExamResponseDto> findById(String id) {
        return Mono.fromCallable(() -> examRepository.findById(id).orElse(null))
                .map(EntityDtoUtil::getExamResponseDto)
                .subscribeOn(Schedulers.boundedElastic());
    }

    public Mono<ExamResponseDto> save(ExamRequestDto request) {
        return Mono.fromCallable(() -> examRepository.save(convert(request)))
                .map(EntityDtoUtil::getExamResponseDto)
                .subscribeOn(Schedulers.boundedElastic());
    }

    public Mono<ExamResponseDto> update(String id, ExamRequestDto request) {
        return Mono.fromCallable(() -> callUpdate(id, request))
                .map(EntityDtoUtil::getExamResponseDto)
                .subscribeOn(Schedulers.boundedElastic());
    }

    public Mono<Void> delete(String id) {
        return Mono.fromRunnable(() -> callExclusion(id))
                .then();
    }

    private void callExclusion(String id) {
        var examConsult = examRepository.findById(id);
        examConsult.ifPresent(this.examRepository::delete);
    }

    private Exam convert(ExamRequestDto request) {
        var exam = Exam.create(
                UUID.randomUUID().toString(),
                request.getName(),
                request.getNumberOfQuestions(),
                request.getPassingGrade(),
                null);

        var examAvailabilities = setAvailabilityIfInformed(exam, request.getAvailabilities());
        exam.setExamAvaliabilities(examAvailabilities);
        return exam;
    }

    private List<ExamAvaliability> setAvailabilityIfInformed(Exam exam, List<AvailabilityRequest> availabilities) {
        if (nonNull(availabilities)) {
            return availabilities.stream().map(availabilityRequest -> convertAvailability(exam, availabilityRequest.getId())).collect(Collectors.toList());
        }
        return null;
    }

    private ExamAvaliability convertAvailability(Exam exam, String id) {
        var availability = availabilityRepository.findById(id).orElse(null);

        if (nonNull(availability)) {
            return ExamAvaliability.create(
                    ExamAvailabilityId.builder()
                            .exam(exam)
                            .availability(availability)
                            .build(),
                    LocalDateTime.now()
            );
        }
        return null;
    }

    private Exam callUpdate(String id, ExamRequestDto request) {
        return examRepository.findById(id)
                .map(exam -> Exam.create(
                        exam.getId(),
                        request.getName(),
                        request.getNumberOfQuestions(),
                        request.getPassingGrade(),
                        getExamAvailabities(exam, request.getAvailabilities())))
                .map(this.examRepository::save)
                .orElse(null);
    }

    private List<ExamAvaliability> getExamAvailabities(Exam exam,
                                                       List<AvailabilityRequest> availabilities) {
        if (nonNull(availabilities)) {
            return setAvailabilityIfInformed(exam, availabilities);
        }
        return exam.getExamAvaliabilities();
    }
}
