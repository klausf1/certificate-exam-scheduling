package com.microservice.api.service;

import com.microservice.api.domain.Room;
import com.microservice.api.dto.room.RoomRequestDto;
import com.microservice.api.dto.room.RoomResponseDto;
import com.microservice.api.repository.RoomRepository;
import com.microservice.api.util.EntityDtoUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
@Transactional
public class RoomService {

    private final RoomRepository roomRepository;

    public Mono<List<RoomResponseDto>> findAll() {
        return Flux.fromIterable(roomRepository.findAll())
                .map(EntityDtoUtil::getRoomResponseDto)
                .collectList()
                .subscribeOn(Schedulers.boundedElastic());
    }

    public Mono<RoomResponseDto> findById(String id) {
        return Mono.fromCallable(() -> roomRepository.findById(id).orElse(null))
                .map(EntityDtoUtil::getRoomResponseDto)
                .subscribeOn(Schedulers.boundedElastic());
    }

    public Mono<RoomResponseDto> save(RoomRequestDto request) {
        return Mono.fromCallable(() -> roomRepository.save(convert(request)))
                .map(EntityDtoUtil::getRoomResponseDto)
                .subscribeOn(Schedulers.boundedElastic());
    }

    public Mono<RoomResponseDto> update(String id, RoomRequestDto request) {
        return Mono.fromCallable(() -> callUpdate(id, request))
                .map(EntityDtoUtil::getRoomResponseDto)
                .subscribeOn(Schedulers.boundedElastic());
    }

    public Mono<Void> delete(String id) {
        return Mono.fromRunnable(() -> callExclusion(id))
                .then();
    }

    private Room convert(RoomRequestDto request) {
        return Room.create(
                UUID.randomUUID().toString(),
                request.getName(),
                request.getNumber(),
                request.getCapacity(),
                request.getLocation(),
                null);
    }

    private Room callUpdate(String id, RoomRequestDto request) {
        return roomRepository.findById(id)
                .map(room -> Room.create(
                        room.getId(),
                        request.getName(),
                        request.getNumber(),
                        request.getCapacity(),
                        request.getLocation(),
                        room.getAvailabilities()))
                .map(this.roomRepository::save)
                .orElse(null);
    }

    private void callExclusion(String id) {
        var roomConsult = roomRepository.findById(id);
        roomConsult.ifPresent(this.roomRepository::delete);
    }
}
