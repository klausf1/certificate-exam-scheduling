package com.microservice.api.service;

import com.microservice.api.domain.Availability;
import com.microservice.api.domain.Room;
import com.microservice.api.dto.availability.AvailabilityRequestDto;
import com.microservice.api.dto.availability.AvailabilityResponseDto;
import com.microservice.api.repository.AvailabilityRepository;
import com.microservice.api.repository.RoomRepository;
import com.microservice.api.util.EntityDtoUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.util.List;
import java.util.UUID;

import static java.util.Objects.nonNull;

@Service
@RequiredArgsConstructor
@Transactional
public class AvailabilityService {

    private final AvailabilityRepository availabilityRepository;
    private final RoomRepository roomRepository;

    public Mono<List<AvailabilityResponseDto>> findAll() {
        return Flux.fromIterable(availabilityRepository.findAll())
                .map(EntityDtoUtil::getAvailabilityResponseDto)
                .collectList()
                .subscribeOn(Schedulers.boundedElastic());
    }

    public Mono<AvailabilityResponseDto> findById(String id) {
        return Mono.fromCallable(() -> availabilityRepository.findById(id).orElse(null))
                .map(EntityDtoUtil::getAvailabilityResponseDto)
                .subscribeOn(Schedulers.boundedElastic());
    }

    public Mono<AvailabilityResponseDto> save(AvailabilityRequestDto request) {
        return Mono.fromCallable(() -> availabilityRepository.save(convert(request)))
                .map(EntityDtoUtil::getAvailabilityResponseDto)
                .subscribeOn(Schedulers.boundedElastic());
    }

    public Mono<AvailabilityResponseDto> update(String id, AvailabilityRequestDto request) {
        return Mono.fromCallable(() -> callUpdate(id, request))
                .map(EntityDtoUtil::getAvailabilityResponseDto)
                .subscribeOn(Schedulers.boundedElastic());
    }

    public Mono<Void> delete(String id) {
        return Mono.fromRunnable(() -> callExclusion(id))
                .then();
    }

    private void callExclusion(String id) {
        var availabilityConsult = availabilityRepository.findById(id);
        availabilityConsult.ifPresent(this.availabilityRepository::delete);
    }

    private Availability convert(AvailabilityRequestDto request) {
        return Availability.create(
                UUID.randomUUID().toString(),
                request.getAvailableDate(),
                request.getPeriod(),
                null,
                setRoomIfInformed(request.getRoomId())
                );
    }

    private Room setRoomIfInformed(String roomId) {
        if (nonNull(roomId)) {
            return roomRepository.findById(roomId).orElse(null);
        }
        return null;
    }

    private Availability callUpdate(String id, AvailabilityRequestDto request) {
        return availabilityRepository.findById(id)
                .map(availability -> Availability.create(
                        availability.getId(),
                        request.getAvailableDate(),
                        request.getPeriod(),
                        null,
                        getRoom(availability.getRoom(), request)))
                .map(this.availabilityRepository::save)
                .orElse(null);
    }

    private Room getRoom(Room room, AvailabilityRequestDto request) {
        if (nonNull(request.getRoomId())) {
            return setRoomIfInformed(request.getRoomId());
        }
        return room;
    }
}
