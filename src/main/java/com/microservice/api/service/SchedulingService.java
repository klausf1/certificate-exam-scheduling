package com.microservice.api.service;

import com.microservice.api.app.exception.BusinessException;
import com.microservice.api.domain.Availability;
import com.microservice.api.domain.Candidate;
import com.microservice.api.domain.Exam;
import com.microservice.api.domain.SchedulingExam;
import com.microservice.api.dto.scheduling.SchedulingRequestDto;
import com.microservice.api.dto.scheduling.SchedulingResponseDto;
import com.microservice.api.repository.*;
import com.microservice.api.util.EntityDtoUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
@Transactional
public class SchedulingService {

    private final SchedulingExamRepository schedulingExamRepository;
    private final CandidateRepository candidateRepository;
    private final ExamRepository examRepository;
    private final AvailabilityRepository availabilityRepository;

    public Mono<List<SchedulingResponseDto>> findAll() {
        return Flux.fromIterable(schedulingExamRepository.findAll())
                .map(EntityDtoUtil::getSchedulingResponseDto)
                .collectList()
                .subscribeOn(Schedulers.boundedElastic());
    }

    public Mono<SchedulingResponseDto> findById(String id) {
        return Mono.fromCallable(() -> schedulingExamRepository.findById(id).orElse(null))
                .map(EntityDtoUtil::getSchedulingResponseDto)
                .subscribeOn(Schedulers.boundedElastic());
    }

    public Mono<String> save(SchedulingRequestDto request) {
        return Mono.fromCallable(() -> schedulingExamRepository.save(convert(request)))
                .flatMap(schedulingExam -> Mono.just("Scheduling successful"))
                .subscribeOn(Schedulers.boundedElastic());
    }

    public Mono<Void> delete(String id) {
        return Mono.fromRunnable(() -> callExclusion(id))
                .then();
    }

    private void callExclusion(String id) {
        var schedulingExamConsult = schedulingExamRepository.findById(id);
        schedulingExamConsult.ifPresent(this.schedulingExamRepository::delete);
    }

    private SchedulingExam convert(SchedulingRequestDto request) {
        var candidate = candidateRepository.findById(request.getCandidateId()).orElse(null);
        throwExceptionIfInvalidCandidate(candidate);

        var exam = examRepository.findById(request.getExamId()).orElse(null);
        throwExceptionIfExamIsInvalid(exam);
        throwExceptionIfNoExamAvailable(exam);

        var availability = availabilityRepository.findByAvailableDateIsAndPeriodIs(request.getAvailabilityDate(),
                request.getPeriod()).orElse(null);

        throwExceptionIfNotAvailable(availability);
        throwExceptionIfNotAvailable(exam, availability.getId());

        var schedulingExam = schedulingExamRepository.findByExamDateIsAndPeriodIsAndExamIs(
                request.getAvailabilityDate(), request.getPeriod(), exam).orElse(null);
        throwExceptionIfScheduleIsAlreadyInUse(schedulingExam);

        return SchedulingExam.create(
                UUID.randomUUID().toString(),
                LocalDateTime.now(),
                request.getAvailabilityDate(),
                request.getPeriod(),
                candidate,
                exam,
                availability.getRoom()
        );
    }

    private void throwExceptionIfScheduleIsAlreadyInUse(SchedulingExam schedulingExam) {
        if (schedulingExam != null) {
            throw new BusinessException("Schedule error because availability is already in used");
        }
    }

    private void throwExceptionIfNotAvailable(Availability availability) {
        if (availability == null) {
            throw new BusinessException("There is no availability on the date and period informed");
        }
    }

    private void throwExceptionIfNotAvailable(Exam exam, String availabilityId) {
        boolean isExist = exam.getExamAvaliabilities().stream().anyMatch(examAvaliability -> examAvaliability.getExamAvailabilityId().getAvailability().getId().equals(availabilityId));

        if (!isExist) {
            throw new BusinessException("There is no exam available");
        }
    }

    private void throwExceptionIfNoExamAvailable(Exam exam) {
        if (exam.getExamAvaliabilities() == null) {
            throw new BusinessException("No availability on the given date");
        }
    }

    private void throwExceptionIfExamIsInvalid(Exam exam) {
        if (exam == null) {
            throw new BusinessException("Exam not found");
        }
    }

    private void throwExceptionIfInvalidCandidate(Candidate candidate) {
        if (candidate == null) {
            throw new BusinessException("Candidate not found");
        }
    }
}
