package com.microservice.api.dto.exam;

import lombok.Data;

@Data
public class AvailabilityRequest {

    private String id;
}
