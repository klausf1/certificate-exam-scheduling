package com.microservice.api.dto.exam;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@Builder
public class ExamRequestDto {

    @NotEmpty(message = "required-field")
    private String name;

    @NotNull(message = "required-field")
    private Integer numberOfQuestions;

    @NotNull(message = "required-field")
    private Integer passingGrade;

    private List<AvailabilityRequest> availabilities;
}
