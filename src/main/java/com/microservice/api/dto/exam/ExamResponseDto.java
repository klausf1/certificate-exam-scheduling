package com.microservice.api.dto.exam;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.microservice.api.dto.availability.AvailabilityResponseDto;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ExamResponseDto {

    private String id;
    private String name;
    private Integer numberOfQuestions;
    private Integer passingGrade;
    private List<AvailabilityResponseDto> availabilities;
}
