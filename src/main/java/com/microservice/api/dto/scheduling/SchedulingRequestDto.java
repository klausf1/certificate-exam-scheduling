package com.microservice.api.dto.scheduling;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
@Builder
public class SchedulingRequestDto {

    @NotEmpty(message = "required-field")
    private String candidateId;

    @NotEmpty(message = "required-field")
    private String examId;

    @NotNull(message = "required-field")
    @JsonFormat(pattern="yyyy-MM-dd")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate availabilityDate;

    @NotEmpty(message = "required-field")
    @ApiModelProperty(allowableValues = "MORNING, AFTERNOON, NIGHT", value = "Availability period")
    private String period;
}
