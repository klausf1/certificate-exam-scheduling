package com.microservice.api.dto.scheduling;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.microservice.api.dto.candidate.CandidateResponseDto;
import com.microservice.api.dto.room.RoomResponseDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SchedulingResponseDto {

    private String id;
    private LocalDateTime createdDate;
    private String exam;
    private LocalDate examDate;
    private String period;
    private CandidateResponseDto candidate;
    private RoomResponseDto room;
}
