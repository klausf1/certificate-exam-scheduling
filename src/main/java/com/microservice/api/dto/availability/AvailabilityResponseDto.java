package com.microservice.api.dto.availability;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.microservice.api.dto.room.RoomResponseDto;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AvailabilityResponseDto {

    private String id;
    private LocalDate availableDate;
    private String period;
    private RoomResponseDto room;
}
