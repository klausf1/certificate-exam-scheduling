package com.microservice.api.dto.candidate;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CandidateResponseDto {

    private String id;
    private String documentNumber;
    private String fullName;
    private String username;
    private LocalDate birthDate;
}
