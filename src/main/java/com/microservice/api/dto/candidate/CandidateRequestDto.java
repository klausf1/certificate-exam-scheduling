package com.microservice.api.dto.candidate;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
@Builder
public class CandidateRequestDto {

    @NotEmpty(message = "required-field")
    private String documentNumber;

    @NotEmpty(message = "required-field")
    private String fullName;

    @NotEmpty(message = "required-field")
    private String username;

    @NotNull(message = "required-field")
    @JsonFormat(pattern="yyyy-MM-dd")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate birthDate;
}
