package com.microservice.api.dto.room;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.microservice.api.dto.availability.AvailabilityResponseDto;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RoomResponseDto {

    private String id;
    private String name;
    private Integer number;
    private Integer capacity;
    private String location;
    private List<AvailabilityResponseDto> availabilities;
}
