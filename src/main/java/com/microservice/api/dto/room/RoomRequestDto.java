package com.microservice.api.dto.room;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@Builder
public class RoomRequestDto {

    @NotEmpty(message = "required-field")
    private String name;

    @NotNull(message = "required-field")
    private Integer number;

    @NotNull(message = "required-field")
    private Integer capacity;

    @NotEmpty(message = "required-field")
    private String location;
}
