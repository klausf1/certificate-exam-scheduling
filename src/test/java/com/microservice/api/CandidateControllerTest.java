package com.microservice.api;

import com.microservice.api.app.config.MessageSourceConfig;
import com.microservice.api.controller.CandidateController;
import com.microservice.api.dto.candidate.CandidateRequestDto;
import com.microservice.api.dto.candidate.CandidateResponseDto;
import com.microservice.api.service.CandidateService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;

import java.time.LocalDate;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.csrf;

@Import({MessageSourceConfig.class})
@WebFluxTest(CandidateController.class)
public class CandidateControllerTest {

    private static final String URL_VALID = "/v1/candidate/";
    private static final String URL_INVALID = "/v2/candidate/";

    private CandidateController controller;

    @Autowired
    private WebTestClient webTestClient;

    @MockBean
    private CandidateService service;

    @BeforeEach
    void setup() {
        this.controller = new CandidateController(service);
    }

    @Test
    void test_unauthorized() {
        webTestClient.get()
                .uri(uriBuilder -> uriBuilder
                        .path(URL_VALID)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isUnauthorized();
    }

    @Test
    @WithMockUser(username="api")
    void test_url_invalid() {
        webTestClient.get()
                .uri(uriBuilder -> uriBuilder
                        .path(URL_INVALID)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isNotFound();
    }

    @Test
    @WithMockUser(username="api")
    void test_findAll() {
        var response = buildCandidateResponse("d2edf38b-4131-4f70-9936-3cee26bb0411",
                "123", "TESTE FULL", "teste123");
        given(service.findAll()).willReturn(Mono.just(createResponseList(response)));
        webTestClient.get()
                .uri(uriBuilder -> uriBuilder
                        .path(URL_VALID)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$[0].id").isEqualTo("d2edf38b-4131-4f70-9936-3cee26bb0411")
                .jsonPath("$[0].documentNumber").isEqualTo("123")
                .jsonPath("$[0].fullName").isEqualTo("TESTE FULL")
                .jsonPath("$[0].username").isEqualTo("teste123")
                .jsonPath("$[0].birthDate").isEqualTo("2000-01-01");
    }

    @Test
    @WithMockUser(username="api")
    void test_findById() {
        var response = buildCandidateResponse("d2edf38b-4131-4f70-9936-3cee26bb0411",
                "123", "TESTE FULL", "teste123");
        given(service.findById(any(String.class))).willReturn(Mono.just(response));
        webTestClient.get()
                .uri(uriBuilder -> uriBuilder
                        .path(URL_VALID.concat("123"))
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.id").isEqualTo("d2edf38b-4131-4f70-9936-3cee26bb0411")
                .jsonPath("$.documentNumber").isEqualTo("123")
                .jsonPath("$.fullName").isEqualTo("TESTE FULL")
                .jsonPath("$.username").isEqualTo("teste123")
                .jsonPath("$.birthDate").isEqualTo("2000-01-01");
    }

    @Test
    @WithMockUser(username="api")
    void test_findById_NotFound() {
        given(service.findById(any(String.class))).willReturn(Mono.empty());
        webTestClient.get()
                .uri(uriBuilder -> uriBuilder
                        .path(URL_VALID.concat("123"))
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isNotFound();
    }

    @Test
    @WithMockUser(username="api")
    void test_save() {
        var request = createRequest("123", "TESTE FULL", "teste123", LocalDate.of(2020, 1, 1));
        var response = buildCandidateResponse("d2edf38b-4131-4f70-9936-3cee26bb0411",
                "123", "TESTE FULL", "teste123");
        given(service.save(any(CandidateRequestDto.class))).willReturn(Mono.just(response));
        webTestClient
                .mutateWith(csrf())
                .post()
                .uri(URL_VALID)
                .bodyValue(request)
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isCreated()
                .expectBody()
                .jsonPath("$.id").isEqualTo("d2edf38b-4131-4f70-9936-3cee26bb0411")
                .jsonPath("$.documentNumber").isEqualTo("123")
                .jsonPath("$.fullName").isEqualTo("TESTE FULL")
                .jsonPath("$.username").isEqualTo("teste123")
                .jsonPath("$.birthDate").isEqualTo("2000-01-01");
    }

    @Test
    @WithMockUser(username="api")
    void test_save_erro_required_field() {
        var request = createRequest("123", "TESTE FULL", "", LocalDate.of(2020, 1, 1));
        given(service.save(any(CandidateRequestDto.class))).willReturn(Mono.empty());
        webTestClient
                .mutateWith(csrf())
                .post()
                .uri(URL_VALID)
                .bodyValue(request)
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isBadRequest()
                .expectBody()
                .jsonPath("$.message").isEqualTo("An error occurred while validating the request data")
                .jsonPath("$.details[0].message").isEqualTo("Mandatory field not informed");
    }

    @Test
    @WithMockUser(username="api")
    void test_update() {
        var request = createRequest("1234", "TESTE FULL", "teste123", LocalDate.of(2020, 1, 1));
        var response = buildCandidateResponse("d2edf38b-4131-4f70-9936-3cee26bb0411",
                "1234", "TESTE FULL", "teste123");
        given(service.update(any(String.class), any(CandidateRequestDto.class))).willReturn(Mono.just(response));
        webTestClient
                .mutateWith(csrf())
                .put()
                .uri(URL_VALID.concat("123"))
                .bodyValue(request)
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isNoContent()
                .expectBody()
                .jsonPath("$.id").isEqualTo("d2edf38b-4131-4f70-9936-3cee26bb0411")
                .jsonPath("$.documentNumber").isEqualTo("1234")
                .jsonPath("$.fullName").isEqualTo("TESTE FULL")
                .jsonPath("$.username").isEqualTo("teste123")
                .jsonPath("$.birthDate").isEqualTo("2000-01-01");
    }

    @Test
    @WithMockUser(username="api")
    void test_delete() {
        given(service.delete(any(String.class))).willReturn(Mono.empty());
        webTestClient
                .mutateWith(csrf())
                .delete()
                .uri(URL_VALID.concat("123"))
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectBody();
    }

    private CandidateRequestDto createRequest(String documentNumber,
                                              String fullName,
                                              String username,
                                              LocalDate birthDate) {
        return CandidateRequestDto.builder()
                .documentNumber(documentNumber)
                .fullName(fullName)
                .username(username)
                .birthDate(birthDate)
                .build();
    }

    private List<CandidateResponseDto> createResponseList(CandidateResponseDto candidateResponseDto) {
        return List.of(buildCandidateResponse(candidateResponseDto.getId(),
                candidateResponseDto.getDocumentNumber(), candidateResponseDto.getFullName(), candidateResponseDto.getUsername()));
    }

    private CandidateResponseDto buildCandidateResponse(String id,
                                                        String documentNumber,
                                                        String fullName,
                                                        String username) {
        return CandidateResponseDto.builder()
                .id(id)
                .documentNumber(documentNumber)
                .fullName(fullName)
                .username(username)
                .birthDate(LocalDate.of(2000, 1, 1))
                .build();
    }
}
