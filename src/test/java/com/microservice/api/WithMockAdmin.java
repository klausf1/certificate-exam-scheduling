package com.microservice.api;

import org.springframework.security.test.context.support.WithMockUser;

@WithMockUser(value="api", roles="ADMIN")
public interface WithMockAdmin {
}
