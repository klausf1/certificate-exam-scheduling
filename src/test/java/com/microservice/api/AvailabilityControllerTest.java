package com.microservice.api;

import com.microservice.api.app.config.MessageSourceConfig;
import com.microservice.api.controller.AvailabilityController;
import com.microservice.api.dto.availability.AvailabilityRequestDto;
import com.microservice.api.dto.availability.AvailabilityResponseDto;
import com.microservice.api.service.AvailabilityService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;

import java.time.LocalDate;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.csrf;

@Import({MessageSourceConfig.class})
@WebFluxTest(AvailabilityController.class)
public class AvailabilityControllerTest {

    private static final String URL_VALID = "/v1/availability/";
    private static final String URL_INVALID = "/v2/availability/";

    private AvailabilityController controller;

    @Autowired
    private WebTestClient webTestClient;

    @MockBean
    private AvailabilityService service;

    @BeforeEach
    void setup() {
        this.controller = new AvailabilityController(service);
    }

    @Test
    void test_unauthorized() {
        webTestClient.get()
                .uri(uriBuilder -> uriBuilder
                        .path(URL_VALID)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isUnauthorized();
    }

    @Test
    @WithMockUser(username="api")
    void test_url_invalid() {
        webTestClient.get()
                .uri(uriBuilder -> uriBuilder
                        .path(URL_INVALID)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isNotFound();
    }

    @Test
    @WithMockUser(username="api")
    void test_findAll() {
        given(service.findAll()).willReturn(Mono.just(createResponseList("MORNING")));
        webTestClient.get()
                .uri(uriBuilder -> uriBuilder
                        .path(URL_VALID)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$[0].id").isEqualTo("248e6bdb-cc6c-41f3-83f7-d8dbf643a77d")
                .jsonPath("$[0].availableDate").isEqualTo("2022-10-31")
                .jsonPath("$[0].period").isEqualTo("MORNING");
    }

    @Test
    @WithMockUser(username="api")
    void test_findById() {
        given(service.findById(any(String.class))).willReturn(Mono.just(buildAvailabilityResponse("MORNING")));
        webTestClient.get()
                .uri(uriBuilder -> uriBuilder
                        .path(URL_VALID.concat("123"))
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.id").isEqualTo("248e6bdb-cc6c-41f3-83f7-d8dbf643a77d")
                .jsonPath("$.availableDate").isEqualTo("2022-10-31")
                .jsonPath("$.period").isEqualTo("MORNING");
    }

    @Test
    @WithMockUser(username="api")
    void test_findById_NotFound() {
        given(service.findById(any(String.class))).willReturn(Mono.empty());
        webTestClient.get()
                .uri(uriBuilder -> uriBuilder
                        .path(URL_VALID.concat("123"))
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isNotFound();
    }

    @Test
    @WithMockUser(username="api")
    void test_save() {
        var request = createRequest("MORNING");
        given(service.save(any(AvailabilityRequestDto.class))).willReturn(Mono.just(buildAvailabilityResponse("MORNING")));
        webTestClient
                .mutateWith(csrf())
                .post()
                .uri(URL_VALID)
                .bodyValue(request)
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isCreated()
                .expectBody()
                .jsonPath("$.id").isEqualTo("248e6bdb-cc6c-41f3-83f7-d8dbf643a77d")
                .jsonPath("$.availableDate").isEqualTo("2022-10-31")
                .jsonPath("$.period").isEqualTo("MORNING");
    }

    @Test
    @WithMockUser(username="api")
    void test_save_erro_required_field() {
        var request = createRequest(null);
        given(service.save(any(AvailabilityRequestDto.class))).willReturn(Mono.empty());
        webTestClient
                .mutateWith(csrf())
                .post()
                .uri(URL_VALID)
                .bodyValue(request)
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isBadRequest()
                .expectBody()
                .jsonPath("$.message").isEqualTo("An error occurred while validating the request data")
                .jsonPath("$.details[0].message").isEqualTo("Mandatory field not informed");
    }

    @Test
    @WithMockUser(username="api")
    void test_update() {
        var request = createRequest("NIGHT");
        given(service.update(any(String.class), any(AvailabilityRequestDto.class))).willReturn(Mono.just(buildAvailabilityResponse("NIGHT")));
        webTestClient
                .mutateWith(csrf())
                .put()
                .uri(URL_VALID.concat("123"))
                .bodyValue(request)
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isNoContent()
                .expectBody()
                .jsonPath("$.id").isEqualTo("248e6bdb-cc6c-41f3-83f7-d8dbf643a77d")
                .jsonPath("$.availableDate").isEqualTo("2022-10-31")
                .jsonPath("$.period").isEqualTo("NIGHT");
    }

    @Test
    @WithMockUser(username="api")
    void test_delete() {
        given(service.delete(any(String.class))).willReturn(Mono.empty());
        webTestClient
                .mutateWith(csrf())
                .delete()
                .uri(URL_VALID.concat("123"))
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectBody();
    }

    private AvailabilityRequestDto createRequest(String period) {
        return AvailabilityRequestDto.builder()
                .availableDate(LocalDate.of(2022, 10, 31))
                .period(period)
                .build();
    }

    private List<AvailabilityResponseDto> createResponseList(String period) {
        return List.of(buildAvailabilityResponse(period));
    }

    private AvailabilityResponseDto buildAvailabilityResponse(String period) {
        return AvailabilityResponseDto.builder()
                .id("248e6bdb-cc6c-41f3-83f7-d8dbf643a77d")
                .availableDate(LocalDate.of(2022, 10, 31))
                .period(period)
                .build();
    }
}
