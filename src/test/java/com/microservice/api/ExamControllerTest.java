package com.microservice.api;

import com.microservice.api.app.config.MessageSourceConfig;
import com.microservice.api.controller.ExamController;
import com.microservice.api.dto.exam.ExamRequestDto;
import com.microservice.api.dto.exam.ExamResponseDto;
import com.microservice.api.service.ExamService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;

import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.csrf;

@Import({MessageSourceConfig.class})
@WebFluxTest(ExamController.class)
public class ExamControllerTest {

    private static final String URL_VALID = "/v1/exam/";
    private static final String URL_INVALID = "/v2/exam/";

    private ExamController controller;

    @Autowired
    private WebTestClient webTestClient;

    @MockBean
    private ExamService service;

    @BeforeEach
    void setup() {
        this.controller = new ExamController(service);
    }

    @Test
    void test_unauthorized() {
        webTestClient.get()
                .uri(uriBuilder -> uriBuilder
                        .path(URL_VALID)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isUnauthorized();
    }

    @Test
    @WithMockUser(username="api")
    void test_url_invalid() {
        webTestClient.get()
                .uri(uriBuilder -> uriBuilder
                        .path(URL_INVALID)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isNotFound();
    }

    @Test
    @WithMockUser(username="api")
    void test_findAll() {
        var response = buildExamResponse("248e6bdb-cc6c-41f3-83f7-d8dbf643a77d", "JAVA", 100, 80);
        given(service.findAll()).willReturn(Mono.just(List.of(response)));
        webTestClient.get()
                .uri(uriBuilder -> uriBuilder
                        .path(URL_VALID)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$[0].id").isEqualTo("248e6bdb-cc6c-41f3-83f7-d8dbf643a77d")
                .jsonPath("$[0].name").isEqualTo("JAVA")
                .jsonPath("$[0].numberOfQuestions").isEqualTo("100")
                .jsonPath("$[0].passingGrade").isEqualTo("80");
    }

    @Test
    @WithMockUser(username="api")
    void test_findById() {
        var response = buildExamResponse("248e6bdb-cc6c-41f3-83f7-d8dbf643a77d", "JAVA", 100, 80);
        given(service.findById(any(String.class))).willReturn(Mono.just(response));
        webTestClient.get()
                .uri(uriBuilder -> uriBuilder
                        .path(URL_VALID.concat("123"))
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.id").isEqualTo("248e6bdb-cc6c-41f3-83f7-d8dbf643a77d")
                .jsonPath("$.name").isEqualTo("JAVA")
                .jsonPath("$.numberOfQuestions").isEqualTo("100")
                .jsonPath("$.passingGrade").isEqualTo("80");
    }

    @Test
    @WithMockUser(username="api")
    void test_findById_NotFound() {
        given(service.findById(any(String.class))).willReturn(Mono.empty());
        webTestClient.get()
                .uri(uriBuilder -> uriBuilder
                        .path(URL_VALID.concat("123"))
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isNotFound();
    }

    @Test
    @WithMockUser(username="api")
    void test_save() {
        var request = createRequest("JAVA", 100, 80);
        var response = buildExamResponse("248e6bdb-cc6c-41f3-83f7-d8dbf643a77d", "JAVA", 100, 80);
        given(service.save(any(ExamRequestDto.class))).willReturn(Mono.just(response));
        webTestClient
                .mutateWith(csrf())
                .post()
                .uri(URL_VALID)
                .bodyValue(request)
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isCreated()
                .expectBody()
                .jsonPath("$.id").isEqualTo("248e6bdb-cc6c-41f3-83f7-d8dbf643a77d")
                .jsonPath("$.name").isEqualTo("JAVA")
                .jsonPath("$.numberOfQuestions").isEqualTo("100")
                .jsonPath("$.passingGrade").isEqualTo("80");
    }

    @Test
    @WithMockUser(username="api")
    void test_save_erro_required_field() {
        var request = createRequest("JAVA", null, 80);

        given(service.save(any(ExamRequestDto.class))).willReturn(Mono.empty());
        webTestClient
                .mutateWith(csrf())
                .post()
                .uri(URL_VALID)
                .bodyValue(request)
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isBadRequest()
                .expectBody()
                .jsonPath("$.message").isEqualTo("An error occurred while validating the request data")
                .jsonPath("$.details[0].message").isEqualTo("Mandatory field not informed");
    }

    @Test
    @WithMockUser(username="api")
    void test_update() {
        var request = createRequest("JAVA", 100, 80);
        var response = buildExamResponse("248e6bdb-cc6c-41f3-83f7-d8dbf643a77d", "JAVA", 100, 80);

        given(service.update(any(String.class), any(ExamRequestDto.class))).willReturn(Mono.just(response));
        webTestClient
                .mutateWith(csrf())
                .put()
                .uri(URL_VALID.concat("123"))
                .bodyValue(request)
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isNoContent()
                .expectBody()
                .jsonPath("$.id").isEqualTo("248e6bdb-cc6c-41f3-83f7-d8dbf643a77d")
                .jsonPath("$.name").isEqualTo("JAVA")
                .jsonPath("$.numberOfQuestions").isEqualTo("100")
                .jsonPath("$.passingGrade").isEqualTo("80");
    }

    @Test
    @WithMockUser(username="api")
    void test_delete() {
        given(service.delete(any(String.class))).willReturn(Mono.empty());
        webTestClient
                .mutateWith(csrf())
                .delete()
                .uri(URL_VALID.concat("123"))
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectBody();
    }

    private ExamRequestDto createRequest(String name, Integer numberOfQuestions, Integer passingGrade) {
        return ExamRequestDto.builder()
                .name(name)
                .numberOfQuestions(numberOfQuestions)
                .passingGrade(passingGrade)
                .build();
    }

    private List<ExamResponseDto> createResponseList(String id, String name, Integer numberOfQuestions, Integer passingGrade) {
        return List.of(buildExamResponse(id, name, numberOfQuestions, passingGrade));
    }

    private ExamResponseDto buildExamResponse(String id,
                                              String name,
                                              Integer numberOfQuestions,
                                              Integer passingGrade) {
        return ExamResponseDto.builder()
                .id(id)
                .name(name)
                .numberOfQuestions(numberOfQuestions)
                .passingGrade(passingGrade)
                .build();
    }
}
