package com.microservice.api;

import com.microservice.api.app.config.MessageSourceConfig;
import com.microservice.api.controller.RoomController;
import com.microservice.api.dto.room.RoomRequestDto;
import com.microservice.api.dto.room.RoomResponseDto;
import com.microservice.api.service.RoomService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;

import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.csrf;

@Import({MessageSourceConfig.class})
@WebFluxTest(RoomController.class)
public class RoomControllerTest {

    private static final String URL_VALID = "/v1/room/";
    private static final String URL_INVALID = "/v2/room/";

    private RoomController controller;

    @Autowired
    private WebTestClient webTestClient;

    @MockBean
    private RoomService service;

    @BeforeEach
    void setup() {
        this.controller = new RoomController(service);
    }

    @Test
    void test_unauthorized() {
        webTestClient.get()
                .uri(uriBuilder -> uriBuilder
                        .path(URL_VALID)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isUnauthorized();
    }

    @Test
    @WithMockUser(username="api")
    void test_url_invalid() {
        webTestClient.get()
                .uri(uriBuilder -> uriBuilder
                        .path(URL_INVALID)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isNotFound();
    }

    @Test
    @WithMockUser(username="api")
    void test_findAll() {
        var response = buildRoomResponse("d2edf38b-4131-4f70-9936-3cee26bb0411",
                "ROOM-X", 3, 5, "TI");
        given(service.findAll()).willReturn(Mono.just(createResponseList(response)));
        webTestClient.get()
                .uri(uriBuilder -> uriBuilder
                        .path(URL_VALID)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$[0].id").isEqualTo("d2edf38b-4131-4f70-9936-3cee26bb0411")
                .jsonPath("$[0].name").isEqualTo("ROOM-X")
                .jsonPath("$[0].number").isEqualTo("3")
                .jsonPath("$[0].capacity").isEqualTo("5")
                .jsonPath("$[0].location").isEqualTo("TI");
    }

    @Test
    @WithMockUser(username="api")
    void test_findById() {
        var response = buildRoomResponse("d2edf38b-4131-4f70-9936-3cee26bb0411",
                "ROOM-X", 3, 5, "TI");
        given(service.findById(any(String.class))).willReturn(Mono.just(response));
        webTestClient.get()
                .uri(uriBuilder -> uriBuilder
                        .path(URL_VALID.concat("123"))
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.id").isEqualTo("d2edf38b-4131-4f70-9936-3cee26bb0411")
                .jsonPath("$.name").isEqualTo("ROOM-X")
                .jsonPath("$.number").isEqualTo("3")
                .jsonPath("$.capacity").isEqualTo("5")
                .jsonPath("$.location").isEqualTo("TI");
    }

    @Test
    @WithMockUser(username="api")
    void test_findById_NotFound() {
        given(service.findById(any(String.class))).willReturn(Mono.empty());
        webTestClient.get()
                .uri(uriBuilder -> uriBuilder
                        .path(URL_VALID.concat("123"))
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isNotFound();
    }

    @Test
    @WithMockUser(username="api")
    void test_save() {
        var request = createRequest("ROOM-X", 3, 5, "TI");
        var response = buildRoomResponse("d2edf38b-4131-4f70-9936-3cee26bb0411",
                "ROOM-X", 3, 5, "TI");
        given(service.save(any(RoomRequestDto.class))).willReturn(Mono.just(response));
        webTestClient
                .mutateWith(csrf())
                .post()
                .uri(URL_VALID)
                .bodyValue(request)
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isCreated()
                .expectBody()
                .jsonPath("$.id").isEqualTo("d2edf38b-4131-4f70-9936-3cee26bb0411")
                .jsonPath("$.name").isEqualTo("ROOM-X")
                .jsonPath("$.number").isEqualTo("3")
                .jsonPath("$.capacity").isEqualTo("5")
                .jsonPath("$.location").isEqualTo("TI");
    }

    @Test
    @WithMockUser(username="api")
    void test_save_erro_required_field() {
        var request = createRequest("ROOM-X", null, 5, "TI");
        given(service.save(any(RoomRequestDto.class))).willReturn(Mono.empty());
        webTestClient
                .mutateWith(csrf())
                .post()
                .uri(URL_VALID)
                .bodyValue(request)
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isBadRequest()
                .expectBody()
                .jsonPath("$.message").isEqualTo("An error occurred while validating the request data")
                .jsonPath("$.details[0].message").isEqualTo("Mandatory field not informed");
    }

    @Test
    @WithMockUser(username="api")
    void test_update() {
        var request = createRequest("ROOM-X", 8, 5, "TI");
        var response = buildRoomResponse("d2edf38b-4131-4f70-9936-3cee26bb0411",
                "ROOM-X", 8, 5, "TI");
        given(service.update(any(String.class), any(RoomRequestDto.class))).willReturn(Mono.just(response));
        webTestClient
                .mutateWith(csrf())
                .put()
                .uri(URL_VALID.concat("123"))
                .bodyValue(request)
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isNoContent()
                .expectBody()
                .jsonPath("$.id").isEqualTo("d2edf38b-4131-4f70-9936-3cee26bb0411")
                .jsonPath("$.name").isEqualTo("ROOM-X")
                .jsonPath("$.number").isEqualTo("8")
                .jsonPath("$.capacity").isEqualTo("5")
                .jsonPath("$.location").isEqualTo("TI");
    }

    @Test
    @WithMockUser(username="api")
    void test_delete() {
        given(service.delete(any(String.class))).willReturn(Mono.empty());
        webTestClient
                .mutateWith(csrf())
                .delete()
                .uri(URL_VALID.concat("123"))
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectBody();
    }

    private RoomRequestDto createRequest(String name,
                                         Integer number,
                                         Integer capacity,
                                         String location) {
        return RoomRequestDto.builder()
                .name(name)
                .number(number)
                .capacity(capacity)
                .location(location)
                .build();
    }

    private List<RoomResponseDto> createResponseList(RoomResponseDto roomResponseDto) {
        return List.of(buildRoomResponse(roomResponseDto.getId(),
                roomResponseDto.getName(), roomResponseDto.getNumber(), roomResponseDto.getCapacity(), roomResponseDto.getLocation()));
    }

    private RoomResponseDto buildRoomResponse(String id,
                                              String name,
                                              Integer number,
                                              Integer capacity,
                                              String location) {
        return RoomResponseDto.builder()
                .id(id)
                .name(name)
                .number(number)
                .capacity(capacity)
                .location(location)
                .build();
    }
}
