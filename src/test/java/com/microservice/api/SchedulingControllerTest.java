package com.microservice.api;

import com.microservice.api.app.config.MessageSourceConfig;
import com.microservice.api.controller.SchedulingController;
import com.microservice.api.dto.candidate.CandidateResponseDto;
import com.microservice.api.dto.scheduling.SchedulingRequestDto;
import com.microservice.api.dto.scheduling.SchedulingResponseDto;
import com.microservice.api.service.SchedulingService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.csrf;

@Import({MessageSourceConfig.class})
@WebFluxTest(SchedulingController.class)
public class SchedulingControllerTest {

    private static final String URL_VALID = "/v1/scheduling/";
    private static final String URL_INVALID = "/v2/scheduling/";

    private SchedulingController controller;

    @Autowired
    private WebTestClient webTestClient;

    @MockBean
    private SchedulingService service;

    @BeforeEach
    void setup() {
        this.controller = new SchedulingController(service);
    }

    @Test
    void test_unauthorized() {
        webTestClient.get()
                .uri(uriBuilder -> uriBuilder
                        .path(URL_VALID)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isUnauthorized();
    }

    @Test
    @WithMockUser(username="api")
    void test_url_invalid() {
        webTestClient.get()
                .uri(uriBuilder -> uriBuilder
                        .path(URL_INVALID)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isNotFound();
    }

    @Test
    @WithMockUser(username="api")
    void test_findAll() {
        given(service.findAll()).willReturn(Mono.just(createResponseList()));
        webTestClient.get()
                .uri(uriBuilder -> uriBuilder
                        .path(URL_VALID)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$[0].id").isEqualTo("d2edf38b-4131-4f70-9936-3cee26bb0411")
                .jsonPath("$[0].createdDate").isEqualTo("2022-06-01T10:10:00")
                .jsonPath("$[0].exam").isEqualTo("JAVA")
                .jsonPath("$[0].examDate").isEqualTo("2022-06-10")
                .jsonPath("$[0].period").isEqualTo("MORNING")
                .jsonPath("$[0].candidate.documentNumber").isEqualTo("123")
                .jsonPath("$[0].candidate.fullName").isEqualTo("TESTE FULL");

    }

    @Test
    @WithMockUser(username="api")
    void test_findById() {
        given(service.findById(any(String.class))).willReturn(Mono.just(buildSchedulingResponse()));
        webTestClient.get()
                .uri(uriBuilder -> uriBuilder
                        .path(URL_VALID.concat("123"))
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.id").isEqualTo("d2edf38b-4131-4f70-9936-3cee26bb0411")
                .jsonPath("$.createdDate").isEqualTo("2022-06-01T10:10:00")
                .jsonPath("$.exam").isEqualTo("JAVA")
                .jsonPath("$.examDate").isEqualTo("2022-06-10")
                .jsonPath("$.period").isEqualTo("MORNING")
                .jsonPath("$.candidate.documentNumber").isEqualTo("123")
                .jsonPath("$.candidate.fullName").isEqualTo("TESTE FULL");
    }

    @Test
    @WithMockUser(username="api")
    void test_findById_NotFound() {
        given(service.findById(any(String.class))).willReturn(Mono.empty());
        webTestClient.get()
                .uri(uriBuilder -> uriBuilder
                        .path(URL_VALID.concat("123"))
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isNotFound();
    }

    @Test
    @WithMockUser(username="api")
    void test_save() {
        var request = createRequest("d2edf38b-4131-4f70-9936-3cee26bb0412",
                "d2edf38b-4131-4f70-9936-3cee26bb0413", LocalDate.of(2022, 6, 10), "MORNING");
        given(service.save(any(SchedulingRequestDto.class))).willReturn(Mono.just("Scheduling successful"));
        webTestClient
                .mutateWith(csrf())
                .post()
                .uri(URL_VALID)
                .bodyValue(request)
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isCreated();
    }

    @Test
    @WithMockUser(username="api")
    void test_save_erro_required_field() {
        var request = createRequest("",
                "d2edf38b-4131-4f70-9936-3cee26bb0413", LocalDate.of(2022, 6, 10), "MORNING");
        given(service.save(any(SchedulingRequestDto.class))).willReturn(Mono.empty());
        webTestClient
                .mutateWith(csrf())
                .post()
                .uri(URL_VALID)
                .bodyValue(request)
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isBadRequest()
                .expectBody()
                .jsonPath("$.message").isEqualTo("An error occurred while validating the request data")
                .jsonPath("$.details[0].message").isEqualTo("Mandatory field not informed");
    }

    @Test
    @WithMockUser(username="api")
    void test_delete() {
        given(service.delete(any(String.class))).willReturn(Mono.empty());
        webTestClient
                .mutateWith(csrf())
                .delete()
                .uri(URL_VALID.concat("123"))
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectBody();
    }

    private SchedulingRequestDto createRequest(String candidateId,
                                               String examId,
                                               LocalDate availabilityDate,
                                               String period) {
        return SchedulingRequestDto.builder()
                .candidateId(candidateId)
                .examId(examId)
                .availabilityDate(availabilityDate)
                .period(period)
                .build();
    }

    private List<SchedulingResponseDto> createResponseList() {
        return List.of(buildSchedulingResponse());
    }

    private SchedulingResponseDto buildSchedulingResponse() {
        return SchedulingResponseDto.builder()
                .id("d2edf38b-4131-4f70-9936-3cee26bb0411")
                .createdDate(LocalDateTime.of(2022, 6, 1, 10, 10, 0))
                .exam("JAVA")
                .examDate(LocalDate.of(2022, 6, 10))
                .period("MORNING")
                .candidate(buildCandidateResponse())
                .build();
    }

    private CandidateResponseDto buildCandidateResponse() {
        return CandidateResponseDto.builder()
                .id("d2edf38b-4131-4f70-9936-3cee26bb0412")
                .documentNumber("123")
                .fullName("TESTE FULL")
                .username("teste123")
                .birthDate(LocalDate.of(2000, 1, 1))
                .build();
    }
}
