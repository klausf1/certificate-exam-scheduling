# certificate-exam-scheduling


API that exposes rest operations related to exam certification scheduling

## Database used
- H2
- username: sa
- password:

## User/Password Spring Security
- username: api
- password: P@sW0rLd
- roles: RO_MICROSERVICES

## Creation of the bank structure
- Liquibase

## Business execution order
- Create a candidate
- Create a room
- Create an availability
- Create an exam
- Scheduling the exam

## Stack used
- Spring Boot
- Java 11
- Spring Security Basic
- JAX-RS
- JSON
- JPA repository
- Liquibase
- Unitary tests
 
## Swagger
[http://localhost:8081/swagger-ui/#/](http://localhost:8081/swagger-ui/#/)
[http://localhost:8081/swagger-ui/index.html?configUrl=/v2/api-docs/swagger-config](http://localhost:8081/swagger-ui/index.html?configUrl=/v2/api-docs/swagger-config)

